package com.sharkbytesystems.lpr;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class OverlayView extends View {
    private Paint mPaint;
    private int mFrameColour;

    public OverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        // Initialize these once for performance rather than calling them every time in onDraw().
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Resources resources = getResources();
        mFrameColour = resources.getColor(R.color.viewfinder_frame);
    }

    public Rect calculateFramingRect() {
        WindowManager manager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point screenResolution =  new Point(display.getWidth(), display.getHeight());
        int width = screenResolution.x * 2 / 5;
        int height = screenResolution.y / 5;
        int leftOffset = (screenResolution.x - width) / 2;
        int topOffset = (screenResolution.y - height) * 2 / 3;
        return new Rect(leftOffset, topOffset, leftOffset + width, topOffset + height);
    }

    @Override
    public void onDraw(Canvas canvas) {
        Rect framingRect = calculateFramingRect();

        // Draw a two pixel solid border inside the framing rect
        mPaint.setAlpha(0);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(mFrameColour);
        canvas.drawRect(framingRect.left, framingRect.top, framingRect.right + 1, framingRect.top + 2, mPaint);
        canvas.drawRect(framingRect.left, framingRect.top + 2, framingRect.left + 2, framingRect.bottom - 1, mPaint);
        canvas.drawRect(framingRect.right - 1, framingRect.top, framingRect.right + 1, framingRect.bottom - 1, mPaint);
        canvas.drawRect(framingRect.left, framingRect.bottom - 1, framingRect.right + 1, framingRect.bottom + 1, mPaint);

    }

    public void drawViewfinder() {
        invalidate();
    }

}
