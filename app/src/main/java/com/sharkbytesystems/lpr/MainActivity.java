package com.sharkbytesystems.lpr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.view.Display;
import android.view.Surface;

import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;
import com.sharkbytesystems.cameralibrary.CameraController;
import com.sharkbytesystems.cameralibrary.CameraFragment;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class MainActivity extends AppCompatActivity implements CameraController.Callback, Camera.PreviewCallback {
    static final int MAX_PLATE_LENGTH = 8;
    static final int MIN_PLATE_LENGTH = 4;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    CameraFragment mCameraFragment;
    final ConcurrentHashMap<LprResult, Integer> mOcrResults = new ConcurrentHashMap<>();

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i("MAIN", "OpenCV loaded successfully");
                    //mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    private boolean mProcessing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CameraController.Params params = CameraController.Params.newInstance()
                .setContainerResId(R.id.camera)
                .setCameraCallback(this)
                .setPreviewCallback(this)
                .setShowCameraControls(false);
        mCameraFragment = CameraController.getInstance().launch(MainActivity.this, params);
        mCameraFragment.setFramingRectPresent(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback);
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onCameraClosed() {
    }

    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera) {
        if (!mProcessing) {
            mProcessing = true;
            Trace myTrace = FirebasePerformance.getInstance().newTrace("detectingImage");
            myTrace.start();
            try {
                Camera.Size size = camera.getParameters().getPreviewSize();
                Bitmap capturedBitmap = ImageUtils.matToBitmap(ImageUtils.yuvImagetoMat(bytes, size, getRotationCompensation()));

                FirebaseVisionImage visionImage = FirebaseVisionImage.fromBitmap(ImageUtils.findLicensePlate(capturedBitmap));
                FirebaseVisionTextRecognizer textRecognizer = FirebaseVision.getInstance().getOnDeviceTextRecognizer();

                textRecognizer.processImage(visionImage)
                        .addOnSuccessListener(MainActivity.this, fvt -> processFirebaseResponse(fvt, capturedBitmap))
                        .addOnFailureListener(MainActivity.this, e -> {
                            Log.d("MAIN", e.getMessage());
                            capturedBitmap.recycle();
                            mProcessing = false;
                        });
            } catch (Exception ex) {
                ex.printStackTrace();
                mProcessing = false;
            } finally {
                myTrace.stop();
            }
        }
    }

    private int getRotationCompensation() {
        int deviceRotation = getWindowManager().getDefaultDisplay().getRotation();
        int rotationCompensation = ORIENTATIONS.get(deviceRotation);

        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);

        int sensorOrientation = info.orientation;
        return (rotationCompensation + sensorOrientation + 270) % 360;
    }

    @Override
    public void onPhotoAccepted(final Bitmap bitmap) {

    }

    private long area(FirebaseVisionText.TextBlock block){
        if(block != null && block.getBoundingBox() != null)
            return (long)(block.getBoundingBox().width() * block.getBoundingBox().height());
        else
            return -1L;
    }

    public void processFirebaseResponse(FirebaseVisionText firebaseVisionText, Bitmap capturedBitmap) {
        List<FirebaseVisionText.TextBlock> textBlocks = StreamSupport.stream(firebaseVisionText.getTextBlocks())
                .filter(block -> block.getText().replaceAll("[^A-Za-z0-9]]", "").length() >= MIN_PLATE_LENGTH
                        && block.getText().replaceAll("[^A-Za-z0-9]]", "").length() <= MAX_PLATE_LENGTH)
                .collect(Collectors.toList());
        if(textBlocks != null && textBlocks.size() > 0){
            Collections.sort(textBlocks, (t, t1) -> Long.compare(area(t1), area(t)));
            updateBestScanResults(LprResult.newInstance(textBlocks.get(0), capturedBitmap));
        } else mProcessing = false;

    }

    void updateBestScanResults(LprResult newResult) {
        synchronized(mOcrResults)
        {
            if(mOcrResults.size() > 0) StreamSupport.stream(mOcrResults.entrySet())
                    .forEach(result -> {
                        String currentText = result.getKey().getText();
                        String newText = newResult.getText().replaceAll("[^A-Za-z0-9]", "");
                        double match = partialStringMatch(newText, currentText);
                        Log.d("MAIN", String.format("%s - %s match %f counter %d",
                                newText,
                                currentText,
                                match,
                                result.getValue()
                        ));
                        if (match >= 0.5D){
                            result.setValue(result.getValue() != null ? result.getValue() + 1 : 1);
                            if(isBetter(currentText, newText)){
                                result.getKey().setText(newText);
                                result.getKey().setBitmap(newResult.getBitmap());
                            }
                        } else {
                            mOcrResults.put(newResult, 0);
                        }
                    });
            else mOcrResults.put(newResult, 0);

            if(findBestOccurrenceNumber() == 3) returnResult();

        } //synchronized(mOcrResults) {

        displayResults(findViewById(R.id.results), false);
        mProcessing = false;
    }

    @Override
    public void onBackPressed() {
        returnResult();
    }

    public void returnResult(){
        int code;
        final LprResult result = findBestResult();
        Intent data = new Intent();
        if(result != null) {
            code = RESULT_OK;
            new Thread(() -> saveImage(result.getText(), result.getBitmap())).start();
            data.putExtra("plate", result.getText() != null ? result.getText() : "");
            data.putExtra("confidence", result.getConfidence());
            data.putExtra("boundingbox", result.getBoundingBox());
        }else{
            code = RESULT_CANCELED;
            LprResult temp = LprResult.newInstance();
            temp.setBitmap(Bitmap.createBitmap(1,1, Bitmap.Config.ALPHA_8));
            data.putExtra("plate", temp.getText() != null ? temp.getText() : "");
            data.putExtra("confidence", temp.getConfidence());
            data.putExtra("boundingbox", temp.getBoundingBox());
        }
        if(getParent() == null) {
            setResult(code, data);
        }else{
            getParent().setResult(code, data);
        }
        Log.d("MAIN", String.format("Returning %s with %d confidence",
                result.getText(),
                result.getConfidence()
        ));
        finish();
    }

    synchronized int findBestOccurrenceNumber(){
        Map.Entry<LprResult, Integer> result = StreamSupport.stream(mOcrResults.entrySet())
                .sorted((x,y) -> Integer.compare(y.getValue(), x.getValue()))
                .findFirst()
                .orElse(null);
        if(result != null){
            return  result.getValue();
        } else return -1;
    }

    synchronized LprResult findBestResult(){
        Map.Entry<LprResult, Integer> result = StreamSupport.stream(mOcrResults.entrySet())
                .sorted((x,y) -> Integer.compare(y.getValue(), x.getValue()))
                .findFirst()
                .orElse(null);
        if(result != null){
            return  result.getKey();
        } else return null;
    }

    synchronized void saveImage(final String textResult, final Bitmap bitmap) {
        FileOutputStream stream = null;
        try {
            String fileName = textResult + ".jpg";

            String path = Environment.getExternalStorageDirectory() + "/mv2/lpr/";
            File dir = new File(path);
            if(!dir.exists()) dir.mkdirs();
            fileName = path + fileName;

            String fileNameGrey = path + textResult + "_grey.jpg";
            Bitmap grey = ImageUtils.findLicensePlate(bitmap);

            stream = new FileOutputStream( fileName );
            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, stream);
            stream.close();

            stream = new FileOutputStream(fileNameGrey);
            grey.compress(Bitmap.CompressFormat.JPEG, 95, stream);
            stream.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stream!=null) stream.close();
            } catch (Throwable et) {
                et.printStackTrace();
            }
        }

    }

    void displayResults(AppCompatTextView ocrResultTextView, boolean displayFinal )
    {
        //display the list of most recent good plate reads
        synchronized(mOcrResults)
        {
            ocrResultTextView.setText("");

            StreamSupport.stream(mOcrResults.entrySet())
                    .forEach(result -> {
                        int conf = result.getKey().getConfidence();

                        String txt = result.getKey().getText();
                        txt += "\n";

                        AppCompatTextView spanbuffer = new AppCompatTextView(this);
                        spanbuffer.setText(txt, AppCompatTextView.BufferType.SPANNABLE);
                        Spannable s = (Spannable) spanbuffer.getText();

                        if (conf > 92) s.setSpan(new ForegroundColorSpan(Color.GREEN), 0, txt.length(), 0);
                        else if (conf > 88) s.setSpan(new ForegroundColorSpan(Color.rgb(128, 255, 0)), 0, txt.length(), 0);
                        else if (conf > 84) s.setSpan(new ForegroundColorSpan(Color.rgb(192, 255, 0)), 0, txt.length(), 0);
                        else if (conf > 80) s.setSpan(new ForegroundColorSpan(Color.rgb(255, 255, 0)), 0, txt.length(), 0);
                        else if (conf > 75) s.setSpan(new ForegroundColorSpan(Color.rgb(255, 192, 0)), 0, txt.length(), 0);
                        else if (conf > 65) s.setSpan(new ForegroundColorSpan(Color.rgb(255, 128, 0)), 0, txt.length(), 0);
                        else s.setSpan(new ForegroundColorSpan(Color.rgb(255, 0, 0)), 0, txt.length(), 0);

                        ocrResultTextView.append(s);
                    });

        } //synchronized(bestScanResults) {


        int scaledSize = 22 + adjustFontsize();
        ocrResultTextView.setTypeface(null, Typeface.NORMAL);
        ocrResultTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, scaledSize);

        if (displayFinal) { //display list larger after scan has completed
            ocrResultTextView.append("\n\n");
            AppCompatTextView spanbuffer = new AppCompatTextView(this);
            String txt = "<paused>";
            spanbuffer.setText(txt, AppCompatTextView.BufferType.SPANNABLE);
            Spannable s = (Spannable) spanbuffer.getText();
            s.setSpan(new ForegroundColorSpan(Color.WHITE), 0, txt.length(), 0);
            ocrResultTextView.append(s);

            ocrResultTextView.setTypeface(null, Typeface.BOLD);
            ocrResultTextView.setTextSize( scaledSize+1 );
        }
    }

    int adjustFontsize() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if ( Math.min(size.x, size.y) <= 600 ) return -5;
        else if ( Math.min(size.x, size.y) <= 800 ) return -3;
        else return 0;
    }

    static boolean isBetter(String p2, String p1) {
        //is plate 1 better than p2?

        int len1 = significantPlateLength(p1);
        int len2 = significantPlateLength(p2);


        return len2 == len1 || ((len2 == MAX_PLATE_LENGTH || len2 == MAX_PLATE_LENGTH - 1) && len2 >= len1 + 1);
    }

    static int significantPlateLength(String p) {
        int len = p.length();
        if (len>0 && (p.charAt(0)=='I' || p.charAt(0)=='1') ) len--;
        if (len>0 && (p.charAt(len-1)=='I' || p.charAt(len-1)=='1') ) len--;
        len = Math.min(MAX_PLATE_LENGTH, len);
        return len;
    }

    public double partialStringMatch(String first, String other) {
        /*possible return values:
        0 -> No match
        1 -> Perfect match
        Everything between - partial match, where values closer to 1 are better
        */

        if(first == null || other == null)
            return 0;

        //if part of a plate is found - sometimes it's happening and L-distance is a no go here
        if (first.contains(other) || other.contains(first))
            return 0.75;

        String longer = first, shorter =other;
        if (longer.length() <shorter.length()) { // longer should always have greater length
            longer = other; shorter = first;
        }
        int longerLength = longer.length();
        if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;
    }

    // Example implementation of the Levenshtein Edit Distance
    // See http://rosettacode.org/wiki/Levenshtein_distance#Java
    public static int editDistance(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0)
                    costs[j] = j;
                else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue),
                                    costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0)
                costs[s2.length()] = lastValue;
        }
        return costs[s2.length()];
    }
}
