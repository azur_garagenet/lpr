package com.sharkbytesystems.lpr;

import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Environment;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

public class ImageUtils {
    public static Mat yuvImagetoMat(byte[] pictureData, Camera.Size pictureSize, int rotationCompensation) {
        Mat yuv = new Mat(pictureSize.height + (pictureSize.height / 2), pictureSize.width, CvType.CV_8UC1);
        yuv.put(0,0, pictureData);
        Mat rgb = new Mat();
        Mat rotated = new Mat();

        Imgproc.cvtColor(yuv, rgb, Imgproc.COLOR_YUV2BGR_NV12);

        int rotationParam = -1;
        switch (rotationCompensation){
            case 0:
                return rgb;
            case 90:
                rotationParam = Core.ROTATE_90_CLOCKWISE;
                break;
            case 180:
                rotationParam = Core.ROTATE_180;
                break;
            case 270:
                rotationParam = Core.ROTATE_90_COUNTERCLOCKWISE;
                break;
        }

        Core.rotate(rgb, rotated, rotationParam);

        return rgb;
    }

    public static Bitmap findLicensePlate(Mat source){
        Mat grey = new Mat();
        Mat blurred = new Mat();
        Mat toOcr = new Mat();

        Mat src = cropLicensePlate(source);

        Imgproc.cvtColor(src, grey, Imgproc.COLOR_BGR2GRAY);

        Imgproc.GaussianBlur(grey, blurred, new Size(3,3), 0);

        Imgproc.adaptiveThreshold(blurred, toOcr, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C , Imgproc.THRESH_BINARY, 99, 4);

        return matToBitmap(toOcr);
    }

    public static Bitmap findLicensePlate(Bitmap source){
        Mat mat = new Mat();
        Utils.bitmapToMat(source, mat);
        return findLicensePlate(mat);
    }

    private static Mat cropLicensePlate(final Mat source){
        int width = BigDecimal.valueOf(source.cols() * 2 / 5).intValue();
        int height = BigDecimal.valueOf(source.rows() / 5).intValue();
        int leftOffset = BigDecimal.valueOf((source.cols() - width) / 2).intValue();
        int topOffset = BigDecimal.valueOf(((source.rows() - height) / 2) + height).intValue();
        return new Mat(source, new Rect(leftOffset, topOffset, width, height));
    }

    public static Bitmap matToBitmap(Mat mat){
        Bitmap bmp;
        bmp = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, bmp);
        return bmp;
    }

    private static double angle(Point p1, Point p2, Point p0) {
        double dx1 = p1.x - p0.x;
        double dy1 = p1.y - p0.y;
        double dx2 = p2.x - p0.x;
        double dy2 = p2.y - p0.y;
        return (dx1 * dx2 + dy1 * dy2)
                / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2)
                + 1e-10);
    }

    public static File saveMatToSDcard(Mat mat){
        return saveBitmapToSDcard(matToBitmap(mat));
    }

    public static File saveBitmapToSDcard(Bitmap bitmap){
        return saveBitmapToSDcard(bitmap, new Date().getTime() + ".png");
    }

    public static File saveBitmapToSDcard(Bitmap bitmap, String name){
        Log.d("IMAGEUTILS", "Starting saving image");
        FileOutputStream fos = null;
        try {
            File dir = new File(Environment.getExternalStorageDirectory().getPath() + "/lpr");
            boolean success = dir.exists() || dir.mkdirs();
            if (success) {
                File output = new File(dir, name);
                fos = new FileOutputStream(output);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                Log.d("IMAGEUTILS", "Finished saving image");
                return dir;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.d("IMAGEUTILS", "Failed to save image");
        return null;
    }

}
