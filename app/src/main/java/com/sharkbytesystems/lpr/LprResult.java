package com.sharkbytesystems.lpr;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.annotation.NonNull;

import com.google.firebase.ml.vision.text.FirebaseVisionText;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;

public class LprResult {
    private Bitmap mBitmap = null;
    private byte[] mBitmapPNGbuffer = null;
    private long mTimeObserved;
    private String mText;
    private Rect mBoundingBox;
    private Point[] mCornerPoints;
    private int mConfidence;

    public static LprResult newInstance(FirebaseVisionText.TextBlock line, Bitmap plate){
        LprResult toReturn = new LprResult();
        toReturn.setText(line.getText().replaceAll("[^A-Za-z0-9]]", ""));
        toReturn.setBitmap(plate);
        toReturn.compressBitmap();
        toReturn.setBoundingBox(line.getBoundingBox());
        toReturn.setCornerPoints(line.getCornerPoints());
        toReturn.setConfidence(BigDecimal.valueOf(line.getConfidence() != null ? line.getConfidence() : 0).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
        return toReturn;
    }

    public static LprResult newInstance() {
        LprResult toReturn = new LprResult();
        toReturn.setText("");
        toReturn.setBitmap(null);
        toReturn.setBoundingBox(new Rect(0,0,0,0));
        toReturn.setCornerPoints(new Point[]{});
        return toReturn;
    }

    private LprResult(){
        setText("");
        setTimeObserved(System.currentTimeMillis());
    }

    public Bitmap getBitmap() {
        return mBitmap != null ? mBitmap : decompressBitmap();
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
        compressBitmap();
    }

    public long getTimeObserved() {
        return mTimeObserved;
    }

    public void setTimeObserved(long timeObserved) {
        mTimeObserved = timeObserved;
    }

    public @NonNull String getText() {
        return mText;
    }

    public void setText(String text) {
        if(text == null) text = "";
        mText = text;
    }

    public Rect getBoundingBox() {
        return mBoundingBox;
    }

    public void setBoundingBox(Rect boundingBox) {
        mBoundingBox = boundingBox;
    }

    public Point[] getCornerPoints() {
        return mCornerPoints;
    }

    public void setCornerPoints(Point[] cornerPoints) {
        mCornerPoints = cornerPoints;
    }

    public int getConfidence() {
        return mConfidence;
    }

    public void setConfidence(int confidence) {
        mConfidence = confidence;
    }

    private void compressBitmap() {
        //compress the bitmap in memory to save RAM
        if (mBitmap == null) return;

        try {
            //see https://stackoverflow.com/questions/7620401/how-to-convert-byte-array-to-bitmap
            ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayBitmapStream);
            mBitmapPNGbuffer = byteArrayBitmapStream.toByteArray();
            recycleBitmap();
        } catch (Throwable e) {
            ;
        }
    }

    private Bitmap decompressBitmap() {
        try {
            //decompress the bitmap
            if (mBitmapPNGbuffer != null) {
                mBitmap = BitmapFactory.decodeByteArray(mBitmapPNGbuffer, 0, mBitmapPNGbuffer.length);
                mBitmapPNGbuffer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mBitmap;
    }

    private void recycleBitmap() {
        if (mBitmap == null) return;
        mBitmap.recycle();   //ensure Bitmap is recycled
        mBitmap = null;
    }
}
